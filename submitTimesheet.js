// All customization options for this script
var config = {
  // Flip this flag to true to set "today" to a fake date (2019-01-18) and
  // also send emails to myself (config.email.debugTo) instead of
  // the real recipients (config.email.to)
  debug: false,

  // A list of all stat holidays for your province.
  // Reminder: months are zero-based in JavaScript (i.e. January = 0)
  // NOTE: these need to be manually updated each year!
  // prettier-ignore
  statHolidays: [
    { name: "New Year's Day",          date: new Date(2019, 0,  1) },
    { name: "Family Day/Islander Day", date: new Date(2019, 1,  18) },
    { name: "Good Friday",             date: new Date(2019, 3,  19) },
    { name: "Victoria Day",            date: new Date(2019, 4,  20) },
    { name: "Canada Day",              date: new Date(2019, 6,  1) },
    { name: "Labour Day",              date: new Date(2019, 8,  2) },
    { name: "Thanksgiving",            date: new Date(2019, 9,  14) },
    { name: "Remembrance Day",         date: new Date(2019, 10, 11) },
    { name: "Christmas Day",           date: new Date(2019, 11, 25) },
  ],

  // A list of days to enter into the spreadsheet as PTO.
  // PTO is still logged as 8 hours - the only difference is that
  // the description field is updated with the text "PTO".
  // Reminder: months are zero-based in JavaScript (i.e. January = 0)
  ptoDates: [
    { date: new Date(2019, 0, 1) },
    { date: new Date(2019, 0, 2) },
    { date: new Date(2019, 0, 3) },
    { date: new Date(2019, 0, 4) },
    { date: new Date(2019, 0, 30) },
    { date: new Date(2019, 0, 31) }
  ],

  // Information about the timesheet email
  email: {
    // Email address(es) of people who should get the generated email and PDF.
    // Separate multiple addresses with a comma
    to: [
      'your-manager@example.com',
      'cxc-contact@example.com',
      'payroll.ca@cxcglobal.com'
    ].join(','),

    // Email address(es) of people who should get the generated email and PDF
    // when in debug mode (instead of the people listed in the regular "to" property).
    debugTo: 'your-email@example.com',

    // Email address(es) of people who should be cc'd
    cc: '',

    // Email address(es) of people who should be cc'd when in debug mode
    debugCc: 'your-email@example.com',

    // Email address(es) of people who should be bcc'd
    bcc: 'your-email@example.com',

    // Email address(es) of people who should be bcc'd when in debug mode
    debugBcc: 'your-email@example.com',

    // A function that returns the subject of the email.  The timesheet
    // date (the Sunday of the week) is provided as a parameter (as a string)
    getSubject: function(dateString) {
      return 'CXC Canada timesheet - week ending ' + dateString;
    },

    // A function that returns the body of the email.  Same parameter as above.
    getBody: function(dateString) {
      return (
        "<p>Hello (manager and CXC contact's name here),</p>" +
        '<p>Attached is my CXC Canada timesheet for the week ending ' +
        dateString +
        '. (manager\'s name), please reply all with "approved" if everything looks good.</p>' +
        '<p>Thanks!</p>' +
        '<p>(your name here)</p>'
      );
    },

    // A function that returns the name of the PDF attachment.  Same parameter as above.
    getAttachmentName: function(dateString) {
      return '(your name here) CXC Timesheet ' + dateString + '.pdf';
    }
  },

  // Information about the email that is sent if this script encounters an error
  errorEmail: {
    // Email address(es) to send the error alert to
    to: 'your-email@example.com',

    // Subject of the error email
    subject: "An error occurred while generating this week's CXC timesheet!",

    // A function that returns the body of the error email. The error object
    // is passed as a parameter.
    getBody: function(err) {
      return (
        '<p>Error details:</p><pre style="color: red">' +
        JSON.stringify(err, null, 4) +
        '</pre>'
      );
    }
  }
};

// Add a few test stat holidays/PTO if we're in debug mode
if (config.debug) {
  config.statHolidays.push({
    name: 'Fake Holiday',
    date: new Date(2019, 0, 16)
  });
  config.ptoDates.push({ date: new Date(2019, 0, 17) });
}

// Trigger the email process to start. If any errors
// occur, send an error email to the specified email address.
function main() {
  try {
    emailSpreadsheetAsPDF();
  } catch (err) {
    // Something went wrong!  Log the error and send an email alert.
    Logger.log(err);

    // The error email details
    var to = config.errorEmail.to;
    var subject = config.errorEmail.subject;
    var body = config.errorEmail.getBody(err);

    GmailApp.sendEmail(to, subject, body, { htmlBody: body });
  }
}

// Change the data in the spreadsheet, and then send the sheet in an email as a PDF
function emailSpreadsheetAsPDF() {
  // Get a reference to the current spreadsheet
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = spreadsheet.getSheets()[0];

  // These variables assume that this script is run on a Friday.
  // This means that Sunday is always today + 2 days.
  var today = config.debug ? new Date(2019, 0, 18) : new Date();
  today.setHours(0, 0, 0, 0);
  var todayAsString = dateToNiceString(today);
  var sunday = today.addDays(2);
  var sundayAsString = dateToNiceString(sunday);

  // Error checking: make sure that today is Friday, because
  // the rest of this script assumes this is true!
  if (today.getDay() !== 5) {
    throw new Error('The script may only be run on a Friday!');
  }

  // Update the timesheet's date to be the upcoming Sunday
  spreadsheet.getRange('J2:K3').setValue(sunday);

  // Update the signature date to be today
  spreadsheet.getRange('I41:K42').setValue(todayAsString);

  // Fill in the timesheet.
  // Normal days = 8 hours, "No description necessary"
  // PTO days = 8 hours, "PTO"
  // Stat holidays = 0 hours, "Stat holiday: <holiday name>"
  for (var row = 17; row <= 21; row++) {
    var cellDate = spreadsheet.getRange('B' + row + ':B' + row).getValue();

    // Test if the current cell refers to a stat holiday
    var holiday = false;
    for (var i = 0; i < config.statHolidays.length; i++) {
      if (isSameDay(cellDate, config.statHolidays[i].date)) {
        holiday = config.statHolidays[i];
      }
    }

    // Test if the current cell refers to a PTO day
    var pto = false;
    for (var i = 0; i < config.ptoDates.length; i++) {
      if (isSameDay(cellDate, config.ptoDates[i].date)) {
        pto = true;
      }
    }

    if (holiday) {
      // If the cell refers to a stat holiday

      spreadsheet.getRange('H' + row + ':H' + row).setValue(0);

      spreadsheet
        .getRange('I' + row + ':I' + row)
        .setValue('Stat holiday: ' + holiday.name)
        .setFontStyle(null);

      spreadsheet.getRange('B' + row + ':K' + row).setBackground('#fff2cc');
    } else if (pto) {
      // If the cell refers to a PTO day

      spreadsheet.getRange('H' + row + ':H' + row).setValue(8);

      spreadsheet
        .getRange('I' + row + ':I' + row)
        .setValue('PTO')
        .setFontStyle(null);

      spreadsheet.getRange('B' + row + ':K' + row).setBackground('#cfe2f3');
    } else {
      // The cell refers to a normal working day

      spreadsheet.getRange('H' + row + ':H' + row).setValue(8);

      spreadsheet
        .getRange('I' + row + ':I' + row)
        .setValue('No description necessary')
        .setFontStyle('italic');

      spreadsheet.getRange('B' + row + ':K' + row).setBackground(null);
    }
  }

  // Make sure all changes are saved
  SpreadsheetApp.flush();

  // Send the PDF of the spreadsheet to email address(es), comma separated
  var email = config.debug ? config.email.debugTo : config.email.to;
  var cc = config.debug ? config.email.debugCc : config.email.cc;
  var bcc = config.debug ? config.email.debugBcc : config.email.bcc;

  // Subject of email message
  var subject = config.email.getSubject(sundayAsString);

  // Body of of email message - formatted with HTML
  var body = config.email.getBody(sundayAsString);

  // Base URL
  var url = 'https://docs.google.com/spreadsheets/d/SS_ID/export?'.replace(
    'SS_ID',
    spreadsheet.getId()
  );

  // Specify PDF export parameters
  // From: https://code.google.com/p/google-apps-script-issues/issues/detail?id=3579
  var url_ext =
    'exportFormat=pdf&format=pdf' + // export as pdf / csv / xls / xlsx
    '&size=letter' + // paper size legal / letter / A4
    '&portrait=true' + // orientation, false for landscape
    '&fitw=true&source=labnol' + // fit to page width, false for actual size
    '&sheetnames=false&printtitle=false' + // hide optional headers and footers
    '&pagenumbers=false&gridlines=false' + // hide page numbers and gridlines
    '&fzr=false' + // do not repeat row headers (frozen rows) on each page
    '&gid='; // the sheet's Id

  // Required for proper permissions
  function setScope() {
    DriveApp.getRootFolder();
  }

  // Set HTTP parameters
  var params = {
    headers: {
      Authorization: 'Bearer ' + ScriptApp.getOAuthToken()
    }
  };

  // Grab the PDF export via full URL
  var response = UrlFetchApp.fetch(url + url_ext + sheet.getSheetId(), params);

  // Update the PDF filename
  var blob = response
    .getBlob()
    .setName(config.email.getAttachmentName(sundayAsString));

  // If allowed to send emails, send the email with the PDF attachment
  if (MailApp.getRemainingDailyQuota() > 0) {
    GmailApp.sendEmail(email, subject, body, {
      cc: cc,
      bcc: bcc,
      htmlBody: body,
      attachments: [blob]
    });
  } else {
    throw new Error(
      'No email could be sent because the remaining daily email quota is 0!'
    );
  }
}

// Converts a date into a string like "2019-01-21"
function dateToNiceString(date) {
  var year = date.getUTCFullYear();
  var month = date.getUTCMonth() + 1;
  var day = date.getUTCDate();

  // Left-pad the digits, if necessary
  month = month < 10 ? '0' + month : month;
  day = day < 10 ? '0' + day : day;

  return [year, month, day].join('-');
}

// Adds a number of days to an existing date.
// From https://stackoverflow.com/a/563442/1063392
Date.prototype.addDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};

// Tests if two dates are the same day (ignoring the time)
// From https://stackoverflow.com/a/43855221/1063392
function isSameDay(d1, d2) {
  return (
    d1.getUTCFullYear() === d2.getUTCFullYear() &&
    d1.getUTCMonth() === d2.getUTCMonth() &&
    d1.getUTCDate() === d2.getUTCDate()
  );
}
