# cxc-canada-scripts 🇨🇦

This repository contains a Google Sheet script that automates the population and submission of weekly CXC Canada timesheets. This script is heavily based on [Cynthia Ng's excellent snippet](https://gitlab.com/snippets/1783173). At a high level, this script does the following:

1. Opens up the Google Sheets timesheet template provided by CXC Canada
1. Edits the "Week Ending" date field to be the upcoming Sunday
1. Updates each row in the main time grid:
   1. For normal working days, the script inserts 8 hours with the description "No description necessary"
   1. For stat holidays, the script inserts 0 hours with the description "Stat holiday: \<holiday name\>". The script highlights this row in yellow.
   1. For PTO, the script inserts 8 hours with the description "PTO". The script highlights this row in blue.
1. Updates the signature date to today
1. Saves the spreadsheet and renders it as a PDF
1. Emails the PDF as an attachment to my manager and my contact at CXC for approval

A completed PDF timesheet looks something like this:

![Screenshot of a completed timesheet](./screenshot.png)

## Setup

To set this Google Sheets script up for yourself:

1. Open up your copy of the CXC Canada Google Sheets timesheet template
1. Fill in all the details that aren't automatically updated by the script
   1. Consultant Name, Client, Location Site, Name, Date
1. Update the spreadsheet's timezone to GMT. This allows the script to accurately deal with time calculations:
   1. File → Spreadsheet settings → General → Time zone → GMT (no daylight saving)
1. Open up the script editor via Tools → Script editor
1. Update the script's timezone to GMT:
   1. File → Project properties → Info → Time zone → GMT (no daylight saving)
1. Paste the contents of [`submitTimesheet.js`](./submitTimesheet.js) into the editor
1. Flip line 6 to `debug: true`
1. Update all data in the `config` variable at the top of the file with your own info.
   1. Make sure you double check which holidays are considered stat holidays - they vary from province to province.
   1. _Note: I'm not sure if listing PTO dates in the CXC timesheet is necessary. I was asked to do it once, so I decided to incorporate this into this script, but I think others don't log their PTO in their timesheets._
1. Verify that the script is working:
   1. In the "Select function" dropdown in the menu bar, select `main`. Then, click the "play" button.

After following these steps, you should receive an email with a short message and a completed timesheet attached. (Note that the date is fixed to 2019-01-18 when `debug === true`.). Carefully read the email and the attached PDF to verify the output is correct. When you're confident everything looks good, flip line 6 back to `debug: false`.

### Running the script every week

To run the script automatically every Friday:

1. Open the script you just created in the script editor and select Edit → Current project's triggers.
1. Select "Add Trigger" in the bottom-right corner of the screen
1. Update the "Select event source" dropdown to "Time-driven"
1. Update the "Select type of time based trigger" dropdown to "Week timer"
1. Update the "Select day of week" dropdown to "Every Friday"
1. Update the "Select time of day" dropdown to the time you'd like your timesheet to be sent. (I chose 2:00 PM.)
1. Click "Save"

## Making it look pretty

There's a few changes you can make to the CXC Google Sheets template to make it a little easier on the eyes:

1. Remove all comments. Comments are appear as "[1]" in the rendered PDF.
2. Remove and re-add borders. Some extra borders show up when using the original template (do a test run to see for yourself).
3. Add an image of your signature. This is totally optional (the signature isn't required), but it makes it look official :smile:.

## Notes

The [`submitTimesheet.js`](./submitTimesheet.js) script is heavily dependent on the structure of the CXC timesheet template. Any changes to the structure of this template will likely break this script.

The script assumes it is running on a Friday. Running the script on any other day will result in an error (unless `config.debug === true`).

If any errors occur while executing the script, the script will send you an email alert with the error details. The details of the error email are specified via `config.errorEmail`.

In its current form, PTO days and holidays must be kept up-to-date by manually editing the script.

Make sure to double-check that you are using the correct stat holiday dates! They vary from province to province.
